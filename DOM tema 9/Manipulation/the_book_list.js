// Exercise 1 The Book List 

let books = [
    {
        title: 'The Design of EveryDay Things',
        author: 'Don Norman',
        alreadyRead: false,
        cover: "https://m.media-amazon.com/images/I/81zpLhP1gWL.jpg"
    }, {
        title: 'The Most Human Human',
        author: 'Brian Christian',
        alreadyRead: true,
        cover: "https://brianchristian.org/images/The-Most-Human-Human-Paperback-Front-Cover.jpg"
    }
];

// create ul

let body = document.body;
let list = document.createElement("ul")

for (let i = 0; i < books.length; i++) {
    // create li
    let bookContent = document.createElement("li");
    let newText = document.createTextNode(books[i].title + " by " + books[i].author);
    bookContent.appendChild(newText);
    list.appendChild(bookContent);
    // change of text color if book is read or not
    if (books[i].alreadyRead == true) {
        bookContent.style.color = "green";
    } else {
        bookContent.style.color = "red";
    }
    // add image next to title and author of book
    let bookImage = document.createElement("img");
    bookImage.src = books[i].cover;
    bookImage.width = "100";
    bookContent.appendChild(bookImage);
}
body.appendChild(list);


