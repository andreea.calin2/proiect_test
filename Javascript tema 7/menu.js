//Exercise 1 - The Fortune Teller
let numberOfChildren = 2;
let partnerName = "Jhon";
let geographicLocation = "Madrid";
let jobTitle = "doctor";

alert(`You will be a ${jobTitle} in ${geographicLocation}, and married to ${partnerName} with ${numberOfChildren} kids.`);

//Exercise 2 - The Age Calculator

let myBirthYear = 1996;
let futureYear = 2024;
let firstPossibleAge = futureYear-myBirthYear;
let secondPossibleAge = firstPossibleAge-1;
alert(`I will be either ${secondPossibleAge} or ${firstPossibleAge} in ${futureYear}.`)

//Exercise 3 - The Lifetime Supply Calculator

let currentAge = 25;
let maxAge = 55;
let estimatedAmount = 5;
const numberOfDaysYear = 365;
let numberOfDays = (maxAge-currentAge)*numberOfDaysYear;
let maxAmount = numberOfDays*estimatedAmount;
alert(`You will need ${maxAmount} to last you until the ripe old age of ${maxAge}.`)

//Exercise 4 - The Geometrizer

const PI = 3.141592653589793;
let radiusOfCircle = 10;
let diameterOfCircle = radiusOfCircle*2;
let circumferenceOfCircle = PI*diameterOfCircle;
let areaOfCircle = PI*radiusOfCircle*radiusOfCircle;
alert(`The circumference is ${circumferenceOfCircle}.`);
alert(`The area is ${areaOfCircle}.`);

//Exercise 5 - The Temperature Converter

const temperatureFormulaCoefficient = 1.8;
const temperatureFormulaConstant = 32;
let givenTemperatureCelsius = 37;
let calculateTemperatureFarenheit = givenTemperatureCelsius*temperatureFormulaCoefficient+temperatureFormulaConstant;
alert(`${givenTemperatureCelsius}°C is ${calculateTemperatureFarenheit}°F.`);
let givenTemperatureFarenheit = 150;
let calculatedTemperatureCelsius = (givenTemperatureFarenheit-temperatureFormulaConstant)/temperatureFormulaCoefficient;
alert(`${givenTemperatureFarenheit}°F is ${calculatedTemperatureCelsius}°C.`);

//Exercise 6 - Space Exercise

6.1

let nameOfSpaceShuttle = "Determination";
let speedOfShuttle = 17500;
const kilometerToMars = 225000000;
const kilometerToMoon = 384400;
const milesPerKilometer = 0.621;

//6.2

let milesToMars = kilometerToMars*milesPerKilometer;
let hoursToMars = milesToMars/speedOfShuttle;
let daysToMars = hoursToMars/24;

//6.3

alert(`${nameOfSpaceShuttle} will take ${daysToMars} days to reach Mars.`)

//6.4

let milesToMoon = kilometerToMoon*milesPerKilometer;
let hoursToMoon = milesToMoon/speedOfShuttle;
let daysToMoon = hoursToMoon/24;
alert(`${nameOfSpaceShuttle} will take ${daysToMoon} days to reach Moon.`)