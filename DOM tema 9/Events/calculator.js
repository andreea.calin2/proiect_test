// create functions to define math operation

function squareNumber(number) {
    let squaredNumber = number * number;
    return squaredNumber;
}

function halfNumber (number) {
    let half = number / 2;
    return half;
}

function percentOfNumber (num1, num2) {
    let percentNumber = num1/num2*100;
    return percentNumber;
}

function areaOfCircle (radius) {
    let areaCircle = radius ** 2 * Math.PI;
    return areaCircle;
}

// add event listener click for every calculate button

// let squareCalc = document.querySelector("#square-button");
// squareCalc.addEventListener("click", function(){
//     let number = document.querySelector("#square-input").value;
//     document.querySelector("#solution").innerHTML = "Result of square number: " + squareNumber(number);
// });

// let halfCalc = document.querySelector("#half-button");
// halfCalc.addEventListener("click", function(){
//     let number = document.querySelector("#half-input").value;
//     document.querySelector("#solution").innerHTML = "Result of half number: " + halfNumber(number);
// });

// let percentCalc = document.querySelector("#percent-button");
// percentCalc.addEventListener("click", function(){
//     let num1 = document.querySelector("#percent1-input").value;
//     let num2 = document.querySelector("#percent2-input").value;
//     document.querySelector("#solution").innerHTML = "Result of percent calculation: " + percentOfNumber(num1, num2);
// });

// let areaCalc = document.querySelector("#area-button");
// areaCalc.addEventListener("click", function(){
//     let radius = document.querySelector("#area-input").value;
//     document.querySelector("#solution").innerHTML = "Result of area: " + areaOfCircle(radius);
// });

// Bonus - add event listener on key press Enter and click

let squareCalcBtn = document.querySelector("#square-button");
    squareCalcBtn.addEventListener("click", function(){
    let number = document.querySelector("#square-input").value;
    document.querySelector("#solution").innerHTML = "Result of square number: " + squareNumber(number);
});

let squareCalc = document.querySelector("#square-input");
squareCalc.addEventListener("keypress", function(event){
    if (event.key === "Enter") {
        let number = document.querySelector("#square-input").value;
        document.querySelector("#solution").innerHTML = "Result of square number: " + squareNumber(number); 
    }
});

let halfCalcBtn = document.querySelector("#half-button");
halfCalcBtn.addEventListener("click", function(){
    let number = document.querySelector("#half-input").value;
    document.querySelector("#solution").innerHTML = "Result of half number: " + halfNumber(number);
});

let halfCalc = document.querySelector("#half-input");
halfCalc.addEventListener("keypress", function(event){
    if (event.key === "Enter") {
        let number = document.querySelector("#half-input").value;
        document.querySelector("#solution").innerHTML = "Result of half number: " + halfNumber(number);
    }
});

let percentCalcBtn = document.querySelector("#percent-button");
percentCalcBtn.addEventListener("click", function(){
    let num1 = document.querySelector("#percent1-input").value;
    let num2 = document.querySelector("#percent2-input").value;
    document.querySelector("#solution").innerHTML = "Result of percent calculation: " + percentOfNumber(num1, num2);
});

let percentCalc = document.querySelector("#percent2-input");
percentCalc.addEventListener("keypress", function(event){
    if (event.key === "Enter") {
        let num1 = document.querySelector("#percent1-input").value;
        let num2 = document.querySelector("#percent2-input").value;
        document.querySelector("#solution").innerHTML = "Result of percent calculation: " + percentOfNumber(num1, num2);
    }
});

let areaCalcBtn = document.querySelector("#area-button");
areaCalcBtn.addEventListener("click", function(){
    let radius = document.querySelector("#area-input").value;
    document.querySelector("#solution").innerHTML = "Result of area: " + areaOfCircle(radius);
});

let areaCalc = document.querySelector("#area-input");
areaCalc.addEventListener("keypress", function(event){
    if (event.key === "Enter") {
        let radius = document.querySelector("#area-input").value;
        document.querySelector("#solution").innerHTML = "Result of area: " + areaOfCircle(radius);
    }
});


