// variable that stores reference to img
let img = document.querySelector("img");

// change img style

img.style.left = "0px";

// create function that makes the img move


// Bonus #1
// function catWalk() {
//     let currentLeft = parseInt(img.style.left);
  
//     if (currentLeft >= (window.innerWidth - img.width)) {
//       img.style.left = "0px"
//     }
//     if (currentLeft < (window.innerWidth - img.width)) {
//         img.style.left = (currentLeft + 10) + 'px';
//     }
//   }

// Bonus #2
let walkForwards = true;
function catWalk() {
  let currentLeft = parseInt(img.style.left);

  if (walkForwards && (currentLeft >= (window.innerWidth - img.width))) {
    walkForwards = false;
  }
  if (!walkForwards && (currentLeft <= 0)) {
    walkForwards = true;
  }

  if (walkForwards) {
    img.style.left = (currentLeft + 10) + 'px';
  } else {
    img.style.left = (currentLeft - 10) + 'px';
  }
}

window.setInterval(catWalk, 50);