function openMobileMenu () {
    document.querySelector(".mobile-navigation-container").classList.add("open");
}
function closeMobileMenu () {
    document.querySelector(".mobile-navigation-container").classList.remove("open");
}