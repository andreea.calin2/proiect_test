// define function for the event listener
let libButton = document.getElementById("lib-button");
function makeMadLib() {
    let noun = document.getElementById("noun").value;
    let adjective = document.getElementById("adjective").value;
    let person = document.getElementById("person").value;
    let story = document.querySelector("#story");
    let storyMade = document.createElement("p");
    storyMade.innerHTML = person + " really likes " + adjective + " " + noun + ".";
    story.appendChild(storyMade);
}

// add event listener on button
libButton.addEventListener( "click", makeMadLib);