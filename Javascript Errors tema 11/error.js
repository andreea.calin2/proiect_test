// Am creat o functie care citeste dintr-un json date despre utilizator
let json = '{"name": "ana", "age": 7}';
function userData(json) {
    let user = JSON.parse(json);
    let userName = user.name;
    let userAge = user.age;

    // functia verifica daca datele exista si afiseaza erorile pentru diferite cazuri
    if ((!userName) && (!userAge)) {
        throw new SyntaxError("Incomplete data: no name and no age")
    }
    if (!userName) {
        throw new SyntaxError("Incomplete data: no name");
    }
    if (!userAge) {
        throw new SyntaxError("Incomplete data: no age");
    }

    //am apelat metoda care verifica tipul de date introdus
    let validation = new ValidationError();
    validation.checkTypeOfData(user);

    //am returnat obiectul user daca toate conditiile sunt false
    console.log(user);
    return user
};

//am definit clasa ValidationError care extinde clasa Error
class ValidationError extends Error {
    constructor(message) {
        super(message);
        this.name = "ValidationError";
    }
    //am definit metoda care verifica tipul de date introdus
    checkTypeOfData(user){
    if (typeof (user.name) != "string") {
        throw new ValidationError("Wrong data type for name.")
    }
    if (typeof (user.age) != "number") {
        throw new ValidationError("Wrong data type for age.")
    }
    }
}
// am apelat functia si am afisat mesajul erorii in consola
try {
    userData(json);
} catch (error) {
    console.log(error);
};



