// Exercise 1 The Fortune Teller

function tellFortune(numberOfChildren, partnerName, geographicLocation, jobTitle){
    console.log(`You will be a ${jobTitle} in ${geographicLocation}, and married to ${partnerName} with ${numberOfChildren} kids.`);
};

tellFortune(2, "Narcisa", "Madrid", "doctor");
tellFortune(5, "Armando", "Malaga", "chef");
tellFortune(0, "Laurentiu", "Barcelona", "programmer");

// Exercise 2 The Puppy Age Calculator

// function calculateDogAge(puppyAge) {
//     let puppyAgeDY = puppyAge*7;
//     console.log(`Your doggie is ${puppyAgeDY} years old in dog years!`);
// }

// calculateDogAge(2);
// calculateDogAge(5);
// calculateDogAge(12);

const rateConversionHumanDog = 7
function calculateDogAge(rateConversionHumanDog, puppyAge) {
    let puppyAgeDogYears = puppyAge*rateConversionHumanDog;
    console.log(`Your doggie is ${puppyAgeDogYears} years old in dog years!`)
};

calculateDogAge(rateConversionHumanDog, 2);
calculateDogAge(rateConversionHumanDog, 5);
calculateDogAge(rateConversionHumanDog, 12);

// Exercise 3 The Lifetime Supply Calculator

const maxAge = 100;
function calculateSupply(age, dailyAmount) {
let maxAmount=Math.round((maxAge-age)*dailyAmount*365);
console.log(`You will need ${maxAmount} to last you until the ripe old age of ${maxAge}`)
};

calculateSupply(25, 2);
calculateSupply(26, 4);
calculateSupply(35, 7.75);

// Exercise 4 The Geometrizer

function calcCircumference(radiusCircle) {
    let circumferenceCircle=radiusCircle*2*Math.PI;
    console.log(`The circumference is ${circumferenceCircle}`);
};

function calcArea(radiusCircle) {
    let areaCircle=radiusCircle ** 2 * Math.PI;
    console.log(`The area is ${areaCircle} `);
};

calcCircumference(3);
calcArea(3);

// Exercise 5 The Temperature Converter

function celsiusToFarenheit(celsiusTemp) {
    let farenheitTemp = celsiusTemp*1.8+32;
    console.log(`${celsiusTemp}°C is ${farenheitTemp}°F`);
}

function farenheitToCelsius(farenheitTemp) {
    let celsiusTemp = (farenheitTemp-32)/1.8;
    console.log(`${farenheitTemp}°F is ${celsiusTemp}°C`);
}

celsiusToFarenheit(37);
farenheitToCelsius(96);

// Exercise 6 The Calculator

// *
function squareNumber(x) {
    let squareNumber = x ** 2;
    console.log(`The result of squaring the number ${x} is ${squareNumber}`);
    return squareNumber
};

squareNumber(3);

// ** 
function halfNumber(x) {
    let halfNumber = x / 2;
    console.log(`Half of ${x} is ${halfNumber}`);
    return halfNumber
};

halfNumber(5);

// *** 
function percentOf(num1, num2) {
    let percentOf = num1/num2*100;
    console.log(`${num1} is ${percentOf}% din ${num2}`);
    return percentOf
};

percentOf(2,4);


// **** 
function areaOfCircle(radiusCircle) {
    let areaCircle = (radiusCircle ** 2 * Math.PI).toFixed(2);
    console.log(`The area of a circle with radius ${radiusCircle} is ${areaCircle}.`);
    return areaCircle
};

areaOfCircle(2);

// *****
function final (x) {
    let half = halfNumber(x);
    let square = squareNumber(half);
    let area = areaOfCircle(square);
    let percent = percentOf(area, square);
}

final(2);

// Exercise 7 

function DrEvil (amount) {
    if (amount == 1000000) {
        return amount + " dollars (pinky)";
    } else {
        return amount + " dollars";
    }
}

// Exercise 8

function mixUp (string1, string2) {
    let result = string2.slice(0, 2) + string1.slice(2) + " " + string1.slice(0, 2) + string2.slice(2);
    return result
}

// Exercise 9 

function fixStart (abc) {
    let firstCharacter = abc.slice(0, 1)
    let restOfCharacters = abc.slice(1).replace(new RegExp(firstCharacter, 'g'), "*");
    let result = firstCharacter + restOfCharacters;
    console.log(result);
    return result
}

// Exercise 10

function verbing (abc) {
    if (abc.length < 3 ) 
        return abc;
    if (abc.slice(-3) == "ing") {
        return word + "ly";
    } else {
        return abc + "ing"; 
    }
}

// Exercise 11 

function notBad (abc) {
    var notIndex = abc.indexOf('not');
    var badIndex = abc.indexOf('bad');
    if (notIndex == -1 || badIndex == -1 || badIndex < notIndex) {
        return abc;
    } else {
        return abc.slice(0, notIndex) + 'good' + abc.slice(badIndex + 3);
    } 
}

