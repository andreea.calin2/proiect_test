// Exercise 1 The recipe card

const favoriteRecipe = {
    title: "Mole",
    servings: 2,
    ingredients: ["cinamon", "cumin", "cocoa"]
}

console.log(favoriteRecipe.title);
console.log("Serves: " + favoriteRecipe.servings);
console.log("Ingredients: ");
for (let i = 0; i < favoriteRecipe.ingredients.length; i++) {
    console.log(favoriteRecipe.ingredients[i]);
};

// Exercise 2 The reading list 

let books = [
    {
        title: "Amintiri din copilarie",
        author: "Ion Creanga",
        alreadyRead: true
    },
    {
        title: "Enigma Otiliei",
        author: "George Calinescu",
        alreadyRead: true
    },
    {
        title: "Morometii",
        author: "Marin Preda",
        alreadyRead: false
    }
]

for (let i = 0; i < books.length; i++) {
    console.log(`${books[i].title} by ${books[i].author}`);
}

for (let i = 0; i < books.length; i++) {
    if (books[i].alreadyRead == true) {
        console.log(`You already read ${books[i].title} by ${books[i].author}`);
    } else {
        console.log(`You still need to read ${books[i].title} by ${books[i].author}`);
    }
}

// Exercise 3 The movie database

let movie = {
    title: "Titanic",
    duration: 180,
    stars: "Kate Winslet, Leonardo DiCaprio"
};

function printInfoMovie(movie){
    console.log(`${movie.title} lasts for ${movie.duration} minutes. Stars: ${movie.stars}.`);
};

printInfoMovie (movie);

// Exercise 4 Credit card validation
let validationResult = {};
function validateCreditCard (cardNumber) {
 var ccNumberNoDash = '';
 for (let i = 0; i < cardNumber.length; i++) {
     if (cardNumber[i] !== '-') {
         ccNumberNoDash += cardNumber[i];
     }
 }
 if (ccNumberNoDash.length !== 16) {
    validationResult.valid = false;
    validationResult.number = cardNumber;
    validationResult.error = "wrong lenght";
    return validationResult;
    return false;
 }
 for (let i = 0; i < ccNumberNoDash.length; i++) {
     let currentNumber = ccNumberNoDash[i];
     currentNumber = Number.parseInt(currentNumber);
     if (!Number.isInteger(currentNumber)) {
        validationResult.valid = false;
        validationResult.number = cardNumber;
        validationResult.error = "invalid characters";
        return validationResult;
         return false;
     }
 }
 let obj = {};
 for (let i = 0; i < ccNumberNoDash.length; i++) {
     obj[ccNumberNoDash[i]] = true;
 }
 if (Object.keys(obj).length < 2) {
    validationResult.valid = false;
    validationResult.number = cardNumber;
    validationResult.error = "only one type of number";
    return validationResult;
     return false;
 }

 if (ccNumberNoDash[ccNumberNoDash.length - 1] % 2 !== 0) {
    validationResult.valid = false;
    validationResult.number = cardNumber;
    validationResult.error = "odd final number";
    return validationResult;
     return false;
 }
 var sum = 0;
 for (let i = 0; i < ccNumberNoDash.length; i++) {
     sum += Number(ccNumberNoDash[i]);
 }
 if (sum <= 16) {
    validationResult.valid = false;
    validationResult.number = cardNumber;
    validationResult.error = "sum less than 16";
    return validationResult;
     return false;
 }
 validationResult.valid = true;
 validationResult.number = cardNumber;
 return validationResult;
 return true;
};

