// font-family to body
let body = document.body;
body.style.fontFamily = "Arial, sans-serif";

// change span text

document.querySelector("#nickname").innerHTML = "Andreea";
document.querySelector("#favorites").innerHTML = "chocolate";
document.querySelector("#hometown").innerHTML = "Galati";

// change class of li 
let listItems = document.querySelectorAll("li");
for (let i = 0; i < listItems.length; i++) {
    listItems[i].className = "list-item";
};

// create element img

let imageOfMe = document.createElement("img");
imageOfMe.src = "images/CALIN ANDREEA.jpg";
body.appendChild(imageOfMe);
